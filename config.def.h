/* See LICENSE file for copyright and license details. */

/* staticstatus */
static const int statmonval = 0;
/* appearance */
static const unsigned int systraypinning = 1;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;     /* 0 means no systray */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int user_bh   = 22;       /* bar height*/
static const int showbar            = 0;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */

static const char *fonts[]               = { "envypn:pixelsize=18", "Siji:style=Regular:size=8"};
static const char dmenufont[]            = "JetBrains Mono:size=9";

static char normfgcolor[]                =  "#c6c8d1";
static char normbgcolor[]                =  "#1E2132";
static char normbordercolor[]            =  "#242736";

static char selfgcolor[]                 = "#DEE0EB";
static char selbgcolor[]                 = "#161821";
static char selbordercolor[]             = "#84a0c6";

static
const
char *colors[][3] = {
	/*                fg            bg            border          */
	[SchemeNorm]  = { normfgcolor,  normbgcolor,  normbordercolor },
	[SchemeSel]   = { selfgcolor,   selbgcolor,   selbordercolor  },
};


/* tagging */
static const char *tags[] = { "", "", "", "", "", "", "", "", "" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class                   instance                  title                  tags mask           isfloating             monitor */
	{  "Gimp",                  NULL,                    NULL,                    0,                    1,                    -1},
	{  "xst",                    NULL,                    "float_all",            ~0,/*alltags*/         1,                    -1},
	{  "xst",                    NULL,                    "float",                 0,                    1,                    -1},
	{  "discord",               NULL,                    NULL,                    0,                    0,                     1},
	{  "Spotify",               NULL,                    NULL,                    0,                    0,                     1},
	{  "Steam",                 NULL,                    NULL,                    1<<2,                 0,                     0},
	{  NULL,                    NULL,                    "RTS",                   0,                    0,                    -1},
	{  NULL,                    "Godot_Engine",          NULL,                    0,                    1,                    -1},
	{  "QjackCtl",              NULL,                    NULL,                    0,                    1,                    -1},
	{  "Pavucontrol",           NULL,                    NULL,                    0,                    1,                    -1},
	{  "qt5ct",                 NULL,                    NULL,                    0,                    1,                    -1},
	{  "dolphin",               NULL,                    NULL,                    0,                    1,                    -1},
	{  "UE4Editor",             NULL,                    NULL,                    0,                    0,                    -1},
	{  "KeePassXC",             NULL,                    NULL,                    1<<7,                 0,                     0},
	{  "TorBrowser",            NULL,                    NULL,                    0,                    1,                    -1},
	{  "Emacs",                 NULL,                    NULL,                    1<<3,                 0,                     0},
	/* {  NULL,                    "leagueclientux.exe",    NULL,                    1<<1,                 1,                     0}, */
	{  NULL,                    "lutris",                NULL,                    1<<1,                 0,                     0},
	/* {  NULL,                    "leagueoflegends.exe",   NULL,                    1<<2,                 0,                     0}, */
	{  "Neo_layout_viewer",     NULL,                    NULL,                   ~0,/*alltags*/         1,                    -1},
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

// in 1850:dwm.c is a reference to layouts[2] beeing monocycle
#include "layouts/tatami.c"

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "  ",      tile },    /* first entry is default */
	{ "|+|",      tatami },
	{ "[M]",      monocle },
	{ "  ",      bstack },
	{ "  ",      bstackhoriz },
	{ "><>",      NULL },    /* no layout function means floating behavior */
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      comboview,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      combotag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */

static const char *emotecmd[] = {
	"emotes.sh",
	"-m", dmenumon,
	"-fn", dmenufont,
	"-nb", normbgcolor,
	"-nf", normfgcolor,
	"-sb", selbgcolor,
	"-sf", selfgcolor,
	NULL
};


static const char *dmenucmd[] = { "rofi", "-show", "drun", "-icon-theme", "breeze-dark", NULL };
static const char *termcmd[]  = { "xst", NULL };
static const char *termfloatingcmd[]  = { "xst", "-t", "float", NULL };

static const char *clkcmd[] = {"xst", "-t", "float_all", "-g", "57x9+1405+916", "-e", "clock.sh", NULL};
static const char *gotopcmd[] = {"xst", "-t", "float", "-e", "zsh", "-c", "LANG=en_US.UTF-8 gotop", NULL};
static const char *thunarcmd[] ={"thunar", NULL};

#include <X11/XF86keysym.h>

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
  { ControlMask|Mod1Mask,         XK_Delete, spawn,          SHCMD("$DOTFILES/bin/rofi/powermenu")},
	{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY|Mod1Mask,              XK_Return, spawn,          {.v = termfloatingcmd } },
	{ MODKEY,                       XK_c,      spawn,          {.v = clkcmd } },
	{ MODKEY,                       XK_Escape, spawn,          {.v = gotopcmd}},
	// { MODKEY,                       XK_t,      spawn,          {.v = thunarcmd } },
	{ MODKEY,                       XK_e,      spawn,          {.v = emotecmd }},
	{ MODKEY|ShiftMask,             XK_v,      spawn,          SHCMD("rofi-switch-sink.sh") },
	{ MODKEY|ShiftMask|Mod1Mask,    XK_v,      spawn,          SHCMD("pavucontrol") },
	{ NULL,                         XK_Print,  spawn,          SHCMD("maim -s | xclip -selection clipboard -t image/png") },
	{ ControlMask,                  XK_Print,  spawn,          SHCMD("maim -i $(xdotool getwindowfocus)| xclip -selection clipboard -t image/png") },
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_t,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_r,      focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_t,      movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_r,      movestack,      {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY|Mod1Mask,              XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_d,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_n,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_d,      setcfact,       {.f = -0.25} },
	{ MODKEY|ShiftMask,             XK_n,      setcfact,       {.f = +0.25} },
	{ MODKEY|ShiftMask,             XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_period, killclient,     {0} },
	{ MODKEY|Mod1Mask,              XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY|Mod1Mask,              XK_f,      setlayout,      {.v = &layouts[5]} },
	{ MODKEY|Mod1Mask,              XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_x,      fullscreen,     {0}},
	{ MODKEY|Mod1Mask,              XK_b,      setlayout,      {.v = &layouts[3]} },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_b,      setlayout,      {.v = &layouts[4]} },
	{ MODKEY|Mod1Mask,              XK_y,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY,                       XK_f,      togglefloating, {0} },
	{ MODKEY,                       XK_0,      comboview,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_w,      focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_m,      focusmon,       {.i = +1 } },
	{ Mod1Mask,                     XK_Tab,    focusmon,       {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_w,      tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_m,      tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button1,        sigdwmblocks,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigdwmblocks,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigdwmblocks,   {.i = 3} },
	{ ClkStatusText,        0,              Button4,        sigdwmblocks,   {.i = 4} },
	{ ClkStatusText,        0,              Button5,        sigdwmblocks,   {.i = 5} },
	{ ClkStatusText,        ShiftMask,      Button1,        sigdwmblocks,   {.i = 6} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

static const char* firefoxcmd[] = {"firefox", NULL};
static const char* lutriscmd[] = {"lutris", NULL};
static const char* mccmd[] = {"multimc", NULL};
static const char* emacscmd[] = {"emacs", NULL};

static Key on_empty_keys[] = {
	/* modifier key            function                argument */
	{ 0,        XK_f,          spawn,                  {.v = firefoxcmd } },
	{ 0,        XK_space,      spawn,                  {.v = dmenucmd}},
	{ 0,        XK_l,          spawn,                  {.v = lutriscmd}},
	{ 0,        XK_m,          spawn,                  {.v = mccmd}},
	{ 0,        XK_e,          spawn,                  {.v = emacscmd}},
};
