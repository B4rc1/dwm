{
  description = "My fork of the dwm window manager";

  outputs = { self }: {
    overlay =
      final: prev: {
        dwm = prev.dwm.overrideAttrs (super: {
          src = self;
          buildInputs = super.buildInputs ++ [ prev.harfbuzz ];
        });
      };
  };
}
